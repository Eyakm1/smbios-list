#!/bin/bash
# kylon 2016

# OS X Users: https://www.topbug.net/blog/2013/04/14/install-and-use-gnu-command-line-tools-in-mac-os-x/

if [ -d "dist" ]; then
    rm -Rf dist
fi

mkdir dist

xmls=(`find SMBIOS -type f`);

xmlsS=( $(
    for el in "${xmls[@]}"
    do
        echo "$el"
    done | sort -V) )

echo -e "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n<SMBIOS>" > dist/smbios-list.xml

for xml in "${xmlsS[@]}"
do
    fileName=$(basename "$xml");
    firstChar="${fileName: : 1}"

    if [ "$firstChar" == "." ]; then
        # fucking os x files
        continue;
    else
        sed 1,1d $xml >> dist/smbios-list.xml
    fi
done

echo "</SMBIOS>" >> dist/smbios-list.xml
exit 0
