<?php
$platformdataC = fopen("platformdata.c", "r");
$smbiosData = array();

/*
 * Functions
 */
function genFwReleaseDate($model, $fwRev) {
    $relDate = '';
    $dateFromFwRev = explode('.', $fwRev);
    $model = str_replace(',', '', $model);

    switch ($model) {
       case 'MacBook11':
       case 'MacBook21':
       case 'MacBook31':
       case 'MacBook41':
       case 'MacBook51':
       case 'MacBook52':
       case 'MacBook61':
       case 'MacBook71':
       case 'MacBookPro11':
       case 'MacBookPro12':
       case 'MacBookPro21':
       case 'MacBookPro22':
       case 'MacBookPro31':
       case 'MacBookPro41':
       case 'MacBookPro51':
       case 'MacBookPro52':
       case 'MacBookPro53':
       case 'MacBookPro54':
       case 'MacBookPro55':
       case 'MacBookPro61':
       case 'MacBookPro62':
       case 'MacBookPro71':
       case 'MacBookPro81':
       case 'MacBookPro82':
       case 'MacBookPro83':
       case 'MacBookAir11':
       case 'MacBookAir21':
       case 'MacBookAir31':
       case 'MacBookAir32':
       case 'MacMini11':
       case 'MacMini21':
       case 'MacMini31':
       case 'MacMini41':
       case 'iMac41':
       case 'iMac42':
       case 'iMac51':
       case 'iMac52':
       case 'iMac61':
       case 'iMac71':
       case 'iMac81':
       case 'iMac91':
       case 'iMac101':
       case 'iMac111':
       case 'iMac112':
       case 'iMac113':
       case 'iMac121':
       case 'iMac122':
       case 'MacPro11':
       case 'MacPro21':
       case 'MacPro31':
       case 'MacPro41':
       case 'MacPro51':
       case 'Xserve11':
       case 'Xserve21':
       case 'Xserve31': //YY
           $relDate = substr($dateFromFwRev[4], 2, 2).'/'.
                       substr($dateFromFwRev[4], 4, 2).'/'.
                       substr($dateFromFwRev[4], 0, 2);
           break;
      default: //YYYY
          $relDate = substr($dateFromFwRev[4], 2, 2).'/'.
              substr($dateFromFwRev[4], 4, 2).'/'.
              '20'.substr($dateFromFwRev[4], 0, 2);
          break;
    }

    return $relDate;
}

/*
 * Parsing
 */
if ($platformdataC) {
    $startParse = false;
    $lineType = 0;

    echo 'Parsing platformdata.c...'."\n";

    while ( ($line = fgets($platformdataC)) !== false ) {
        $l = trim($line);
        $lSub = substr($l, 0, 2);

        if (!$startParse && substr($l, 0, 8) === "PLATFORM") {
            $startParse = true;
            continue;
        }

        if ($lSub == '//' || $l == "{" || !$startParse)
            continue;
        elseif ($startParse && $lSub == "};")
            break;

        $nl = str_replace(array('{', '}', '"'), '', $l);
        $t = explode(',', $nl);

        switch ($lineType) {
            case 1:
                $isOddFamily = $t[0] == 'iMac17' ? true:false;
                $i = 0;

                $smbiosData[] = $isOddFamily ? $t[$i++].','.$t[$i++] : $t[$i++]; // family
                $smbiosData[] = trim($t[$i++]); // sys ver

                $smbiosData[] = trim(substr($t[$i], 9, strlen($t[$i]))); // model id
                ++$i;

                $smbiosData[] = trim($t[$i]); // chassis
                break;
            case 2:
                $nl = str_replace(array("0x0", "0x", " "), '', $nl);
                $t = explode(',', $nl);

                $smcRev = $t[0].'.'.$t[1].$t[2];

                if ($t[3] !== "0")
                    $smcRev .= $t[3];

                if ($t[4] !== "0")
                    $smcRev .= $t[4];

                $smcRev .= $t[5];

                $smbiosData[] = $smcRev;
                break;
            default: // type 0
                $productName = $t[0].','.$t[1];

                $smbiosData[] = trim($productName);
                $smbiosData[] = trim($t[2]); // fw rev
                $smbiosData[] = trim($t[3]); // board id
                break;
        }

        $lineType = $lineType == 2 ? 0 : $lineType+1;
    }

    fclose($platformdataC);
} else {
    echo "Cannot find platformdata.c\n";
}

/*
 * Update xmls
 */
for ($i=0, $len=count($smbiosData); $i<$len; $i+=8) {
    $folder = preg_replace('/[0-9]+,[0-9]+/', '', $smbiosData[$i]);
    $smbiosXmlPath = "../SMBIOS/".$folder.'/'.strtolower(str_replace(',', '', $smbiosData[$i])).'.xml';

    if (file_exists($smbiosXmlPath)) {
        $smbiosXml = simplexml_load_file($smbiosXmlPath);
        $fwRelDate = genFwReleaseDate($smbiosData[$i], $smbiosData[$i+1]);
        $isChanged = false;

        if ($smbiosXml->BiosReleaseDate != $fwRelDate) {
            $smbiosXml->BiosReleaseDate = $fwRelDate;
            $isChanged = true;
        }

        if ($smbiosXml->ProductName != $smbiosData[$i]) {
            $smbiosXml->ProductName = $smbiosData[$i];
            $isChanged = true;
        }

        if ($smbiosXml->BoardVersion != $smbiosData[$i]) {
            $smbiosXml->BoardVersion = $smbiosData[$i];
            $isChanged = true;
        }

        if ($smbiosXml->BiosVersion != $smbiosData[$i+1]) {
            $smbiosXml->BiosVersion = $smbiosData[$i+1];
            $isChanged = true;
        }

        if ($smbiosXml->BoardID != $smbiosData[$i+2]) {
            $smbiosXml->BoardID = $smbiosData[$i+2];
            $isChanged = true;
        }

        if ($smbiosXml->Family != $smbiosData[$i+3]) {
            $smbiosXml->Family = $smbiosData[$i+3];
            $isChanged = true;
        }

        if ($smbiosXml->Version != $smbiosData[$i+4]) {
            $smbiosXml->Version = $smbiosData[$i+4];
            $isChanged = true;
        }

        if ($smbiosXml->ModelId != $smbiosData[$i+5]) {
            $smbiosXml->ModelId = $smbiosData[$i+5];
            $isChanged = true;
        }

        if ($smbiosXml->ChassisAsset != $smbiosData[$i+6]) {
            $smbiosXml->ChassisAsset = $smbiosData[$i+6];
            $isChanged = true;
        }

        if ($smbiosXml->SmcRevision != $smbiosData[$i+7]) {
            $smbiosXml->SmcRevision = $smbiosData[$i+7];
            $isChanged = true;
        }

        if ($isChanged)
            $smbiosXml->saveXML($smbiosXmlPath);
    } else {
        echo "No xml found for ".$smbiosData[$i]."\n";
    }
}

echo "All done!\n";
exit();
